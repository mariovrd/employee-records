<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods,Authorization,X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Job.php';

// Connect to DB
$database = new Database();
$db = $database->connect();

$job = new Job($db);

// Get posted data
$data = json_decode(file_get_contents("php://input"));

$job->name = $data->name;
$job->description = $data->description;

// Add job
if ($job->add_job()) {
    echo json_encode(
        array('message' => 'Job Added')
    );
} else {
    echo json_encode(
        array('message' => 'Job Not Added')
    );
}
