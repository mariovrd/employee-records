<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/Database.php';
include_once '../../models/Job.php';

// Connect to DB
$database = new Database();
$db = $database->connect();

$job = new Job($db);

// Get ID
$job->id = isset($_GET['id']) ? $_GET['id'] : die();

$job->get_job();

$job_arr = array(
    'id' => $job->id,
    'name' => $job->name,
    'description' => $job->description,
);

// Make JSON
print_r(json_encode($job_arr));

?>