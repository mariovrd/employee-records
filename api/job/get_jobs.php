<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/Database.php';
include_once '../../models/Job.php';

// Connect to DB
$database = new Database();
$db = $database->connect();

$job = new Job($db);

// Job query
$result = $job->get_jobs();

// Get row count
$num = $result->rowCount();

// Check if any jobs
if($num > 0) {
    // Job array
    $jobs_arr = array();
    while($row = $result->fetch(PDO::FETCH_ASSOC)) {
        extract($row);
        $job_item = array(
            'id' => $id,
            'name' => $name,
            'description' => $description
        );
        // Push to "data"
        array_push($jobs_arr, $job_item);
    }
    // Turn to JSON & output
    echo json_encode($jobs_arr);
} else {
    // No Jobs
    echo json_encode(
        array('message' => 'No Jobs Found')
    );
}

?>