<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: PUT');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods,Authorization,X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Job.php';

// Connect to DB
$database = new Database();
$db = $database->connect();

$job = new Job($db);

// Get posted data
$data = json_decode(file_get_contents("php://input"));

// Set ID to update
$job->id = $data->id;

$job->name = $data->name;
$job->description = $data->description;

// Update job
if ($job->update_job()) {
    echo json_encode(
        array('message' => 'Job Updated')
    );
} else {
    echo json_encode(
        array('message' => 'Job Not Updated')
    );
}
