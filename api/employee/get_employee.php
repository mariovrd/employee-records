<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/Database.php';
include_once '../../models/Employee.php';

// Connect to DB
$database = new Database();
$db = $database->connect();

$employee = new Employee($db);

// Get ID
$employee->id = isset($_GET['id']) ? $_GET['id'] : die();

$employee->get_employee();

$employee_arr = array(
    'id' => $employee->id,
    'first_name' => $employee->first_name,
    'last_name' => $employee->last_name,
    'job_id' => $employee->job_id,
    'job_name' => $employee->job_name,
    'employed_ad' => $employee->employed_at
);

// Make JSON
print_r(json_encode($employee_arr));

?>