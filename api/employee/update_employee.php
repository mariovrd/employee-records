<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: PUT');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods,Authorization,X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Employee.php';

// Connect to DB
$database = new Database();
$db = $database->connect();

$employee = new Employee($db);

// Get posted data
$data = json_decode(file_get_contents("php://input"));

// Set ID to update
$employee->id = $data->id;

$employee->first_name = $data->first_name;
$employee->last_name = $data->last_name;
$employee->job_id = $data->job_id;

// Update employee
if ($employee->update_employee()) {
    echo json_encode(
        array('message' => 'Employee Updated')
    );
} else {
    echo json_encode(
        array('message' => 'Employee Not Updated')
    );
}
