<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods,Authorization,X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Employee.php';

// Connect to DB
$database = new Database();
$db = $database->connect();

$employee = new Employee($db);

// Get posted data
$data = json_decode(file_get_contents("php://input"));

$employee->first_name = $data->first_name;
$employee->last_name = $data->last_name;
$employee->job_id = $data->job_id;

// Add employee
if ($employee->add_employee()) {
    echo json_encode(
        array('message' => 'Employee Added')
    );
} else {
    echo json_encode(
        array('message' => 'Employee Not Added')
    );
}
