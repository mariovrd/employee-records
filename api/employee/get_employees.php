<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/Database.php';
include_once '../../models/Employee.php';

// Connect to DB
$database = new Database();
$db = $database->connect();

$employee = new Employee($db);

// Employee query
$result = $employee->get_employees();

// Get row count
$num = $result->rowCount();

// Check if any employees
if($num > 0) {
    // Employee array
    $employees_arr = array();
    while($row = $result->fetch(PDO::FETCH_ASSOC)) {
        extract($row);
        $employee_item = array(
            'id' => $id,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'job_id' => $job_id,
            'job_name' => $job_name,
            'employed_at' => $employed_at
        );
        // Push to "data"
        array_push($employees_arr, $employee_item);
    }
    // Turn to JSON & output
    echo json_encode($employees_arr);
} else {
    // No Employees
    echo json_encode(
        array('message' => 'No Employees Found')
    );
}

?>