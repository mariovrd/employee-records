<?php

class Employee {
    // DB properties
    private $conn;
    private $table = 'employees';

    // Employee properties
    public $id;
    public $job_id;
    public $job_name;
    public $first_name;
    public $last_name;
    public $employed_at;

    public function __construct($db) {
        $this->conn = $db;
    }

    // Get employees
    public function get_employees() {
        $query = 'SELECT j.name as job_name, e.id, e.job_id, e.first_name, e.last_name, e.employed_at
                            FROM ' . $this->table . ' e
                            LEFT JOIN
                                jobs j ON e.job_id = j.id
                            ORDER BY 
                                e.employed_at DESC';
        
        $statement = $this->conn->prepare($query);

        $statement->execute();

        return $statement;
    }

    // Get one employee
    public function get_employee() {
        $query = 'SELECT j.name as job_name, e.id, e.job_id, e.first_name, e.last_name, e.employed_at
                            FROM ' . $this->table . ' e
                            LEFT JOIN
                                jobs j ON e.job_id = j.id
                            WHERE
                                e.id = ?
                            LIMIT 0,1';

        $statement = $this->conn->prepare($query);

        // Bind ID
        $statement->bindParam(1, $this->id);

        // Execute query
        $statement->execute();
        
        $row = $statement->fetch(PDO::FETCH_ASSOC);
        // Set properties
        $this->first_name = $row['first_name'];
        $this->last_name = $row['last_name'];
        $this->job_id = $row['job_id'];
        $this->job_name = $row['job_name'];
                  
    }

    // Add employee
    public function add_employee() {
        $query = 'INSERT INTO ' . $this->table . '
                    SET
                        first_name = :first_name,
                        last_name = :last_name,
                        job_id = :job_id';
        
        $statement = $this->conn->prepare($query);

        // Sanitize data
        $this->first_name = htmlspecialchars(strip_tags($this->first_name));
        $this->last_name = htmlspecialchars(strip_tags($this->last_name));
        $this->job_id = htmlspecialchars(strip_tags($this->job_id));

        // Bind data
        $statement->bindParam(':first_name', $this->first_name);
        $statement->bindParam(':last_name', $this->last_name);
        $statement->bindParam(':job_id', $this->job_id);

        if ($statement->execute()) {
            return true;
        }

        printf("Error: %s.\n", $statement->error);
        
        return false;
    }

    // Update employee
    public function update_employee() {
        $query = 'UPDATE ' . $this->table . '
                    SET
                        first_name = :first_name,
                        last_name = :last_name,
                        job_id = :job_id
                    WHERE
                        id = :id';
        
        $statement = $this->conn->prepare($query);

        // Sanitize data
        $this->first_name = htmlspecialchars(strip_tags($this->first_name));
        $this->last_name = htmlspecialchars(strip_tags($this->last_name));
        $this->job_id = htmlspecialchars(strip_tags($this->job_id));
        $this->id = htmlspecialchars(strip_tags($this->id));

        // Bind data
        $statement->bindParam(':first_name', $this->first_name);
        $statement->bindParam(':last_name', $this->last_name);
        $statement->bindParam(':job_id', $this->job_id);
        $statement->bindParam(':id', $this->id);

        if ($statement->execute()) {
            return true;
        }

        printf("Error: %s.\n", $statement->error);
        
        return false;
    }

    // Remove Employee
    public function remove_employee() {
        $query = 'DELETE FROM ' . $this->table . ' WHERE id = :id';

        $statement = $this->conn->prepare($query);

        $this->id = htmlspecialchars(strip_tags($this->id));

        $statement->bindParam(':id', $this->id);

        if ($statement->execute()) {
            return true;
        }

        printf("Error: %s.\n", $statement->error);
        
        return false;
    }
}

?>