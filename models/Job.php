<?php 

class Job {
    private $conn;
    private $table = 'jobs';

    public $id;
    public $name;
    public $description;

    public function __construct($db) {
        $this->conn = $db;
    }

    public function get_jobs() {
        $query = 'SELECT j.id, j.name, j.description 
                    FROM
                        '. $this->table .' j
                    ORDER BY j.name DESC';
        
        $statement = $this->conn->prepare($query);

        $statement->execute();

        return $statement;

    }

    // Get one job
    public function get_job() {
        $query = 'SELECT j.id, j.name, j.description
                            FROM ' . $this->table . ' j
                            WHERE
                                j.id = ?
                            LIMIT 0,1';

        $statement = $this->conn->prepare($query);

        // Bind ID
        $statement->bindParam(1, $this->id);

        // Execute query
        $statement->execute();
        
        $row = $statement->fetch(PDO::FETCH_ASSOC);
        // Set properties
        $this->id = $row['id'];
        $this->name = $row['name'];
        $this->description = $row['description'];
                    
    }

    // Add job
    public function add_job() {
        $query = 'INSERT INTO ' . $this->table . '
                    SET
                        name = :name,
                        description = :description';
        
        $statement = $this->conn->prepare($query);

        // Sanitize data
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->description = htmlspecialchars(strip_tags($this->description));

        // Bind data
        $statement->bindParam(':name', $this->name);
        $statement->bindParam(':description', $this->description);

        if ($statement->execute()) {
            return true;
        }

        printf("Error: %s.\n", $statement->error);
        
        return false;
    }
    
    // Update job
    public function update_job() {
        $query = 'UPDATE ' . $this->table . '
                    SET
                        name = :name,
                        description = :description
                    WHERE
                        id = :id';
        
        $statement = $this->conn->prepare($query);

        // Sanitize data
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->description = htmlspecialchars(strip_tags($this->description));
        $this->id = htmlspecialchars(strip_tags($this->id));

        // Bind data
        $statement->bindParam(':name', $this->name);
        $statement->bindParam(':description', $this->description);
        $statement->bindParam(':id', $this->id);

        if ($statement->execute()) {
            return true;
        }

        printf("Error: %s.\n", $statement->error);
        
        return false;
    }

    
    // Remove job
    public function remove_job() {
        $query = 'DELETE FROM ' . $this->table . ' WHERE id = :id';

        $statement = $this->conn->prepare($query);

        $this->id = htmlspecialchars(strip_tags($this->id));

        $statement->bindParam(':id', $this->id);

        if ($statement->execute()) {
            return true;
        }

        printf("Error: %s.\n", $statement->error);
        
        return false;
    }

}

?>